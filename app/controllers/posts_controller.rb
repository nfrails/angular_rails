class PostsController < ApplicationController
  before_filter :authenticate_user!, only: [:create, :upvote]

  def index
    #respond_with Post.all

     @posts = Post.all
     render json: @posts
  end

  def create
    @post = Post.create(post_params.merge(user_id: current_user.id))
    render json: @post
  end

  def show
    post = Post.find(params[:id])
    render json: post
  end

  def upvote
    @post = Post.find(params[:id])
    @post.increment!(:upvotes)

    render json: @post
  end


  private

  def post_params
    params.require(:post).permit(:link, :title, :upvotes)
  end
end
