angular.module('flapperNews')
    .factory('posts', ['$http',function($http){

        var factory = {
            posts: []
        };

        //get all posts from DB, copy to post array, return to resolve function of route
        factory.getAll = function() {
            return $http.get('/posts.json').success(function(data){
                angular.copy(data.posts, factory.posts);
                //console.log(angular.copy(data, factory.posts));
                //factory.posts.push(data);
            });
        };

        factory.getpost = function(id) {
            return $http.get('/posts/' + id + '.json').then(function(res){
                return res.data.post;
            });
        };

        //create new post, and add to post array
        factory.create = function(post) {
            post.upvotes = 0;
            return $http.post('/posts.json', post).success(function(data){
                factory.posts.push(data.post);
            });
        };

        //upvoting posts
        factory.upvote = function(post) {
            return $http.put('/posts/' + post.id + '/upvote.json')
                .success(function(data){
                    post.upvotes += 1;
                });
        };

        factory.addComment = function(id, comment) {
            return $http.post('/posts/' + id + '/comments.json', comment);
            };

        factory.upvoteComment = function(post, comment) {
            return $http.put('/posts/' + post.id + '/comments/'+ comment.id + '/upvote.json')
                .success(function(data){
                    comment.upvotes += 1;
                });
        };


        return factory;
    }]);