    angular.module('flapperNews')
        .controller('PostController',['posts','$scope','post',function(posts,$scope,post){
            $scope.post = post;

            $scope.addComment = function(){
                if($scope.body === '') { return; }
                posts.addComment(post.id, {
                    body: $scope.body,
                    upvotes: 0
                   // author: 'user'
                }).success(function(comment) {
                    $scope.post.comments.push(comment.comment);
                });
                $scope.body = '';
            };

            $scope.incrementUpvotes = function(comment){
                posts.upvoteComment(post, comment);

            };

        }]);
